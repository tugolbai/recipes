import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Recipe } from '../shared/recipe.model';
import { RecipeService } from '../shared/recipe.service';


@Injectable({
  providedIn: 'root'
})

export class RecipeResolverService implements Resolve<Recipe> {
  constructor(private recipeService: RecipeService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Recipe> | Observable<never> {
    const recipeId = <string>route.params['id'];
    return this.recipeService.fetchRecipe(recipeId).pipe(mergeMap(recipeId => {
      if (recipeId) {
        return of(recipeId);
      }
      void this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
