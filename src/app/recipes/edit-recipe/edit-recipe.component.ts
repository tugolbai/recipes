import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from '../../shared/recipe.service';
import { Recipe } from '../../shared/recipe.model';

@Component({
  selector: 'app-edit-recipe',
  templateUrl: './edit-recipe.component.html',
  styleUrls: ['./edit-recipe.component.css']
})
export class EditRecipeComponent implements OnInit, OnDestroy {
  recipeForm!: FormGroup;
  isEdit = false;
  editedId = '';

  isUploading = false;
  recipeUploadingSubscription!: Subscription;

  constructor(
    private recipeService: RecipeService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}


  ngOnInit(): void {
    this.recipeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      imageUrl: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      steps: new FormArray([])
    });

    this.recipeUploadingSubscription = this.recipeService.recipeUploading
      .subscribe((isUploading: boolean) => {
        this.isUploading = isUploading;
      });

    this.route.data.subscribe(data => {
      const recipe = <Recipe | null>data.recipe;
      if (recipe) {
        this.isEdit = true;
        this.editedId = recipe.id;
        this.setFormValue({
          name: recipe.name,
          imageUrl: recipe.imageUrl,
          description: recipe.description,
          ingredients: recipe.ingredients,
        });
        recipe.steps?.forEach(step => {
          const steps = <FormArray>this.recipeForm.get('steps');
          const stepGroup = new FormGroup({
            stepImage: new  FormControl(step.stepImage, Validators.required),
            stepDescription: new FormControl(step.stepDescription, Validators.required)
          });
          steps.push(stepGroup);
        })
      } else {
        this.isEdit = false;
        this.editedId = '';
      }
    });
  }

  setFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.recipeForm.patchValue(value);
    });
  }

  onSubmit() {
    const id = this.editedId || Math.random().toString();
    const recipe = new Recipe(
      id,
      this.recipeForm.get('name')?.value,
      this.recipeForm.get('imageUrl')?.value,
      this.recipeForm.get('description')?.value,
      this.recipeForm.get('ingredients')?.value,
      this.recipeForm.get('steps')?.value,
    );

    const next = () => {
      this.recipeService.fetchRecipes();
    };

    if (this.isEdit) {
      this.recipeService.editRecipe(recipe).subscribe(next);
    } else {
      this.recipeService.addRecipe(recipe).subscribe(next);
      void this.router.navigate(['/']);
    }
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.recipeForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  stepHasError(fieldName: AbstractControl, errorType: string, imageOrDescription: string) {
    const sk = <FormGroup>fieldName;
    return Boolean(sk && sk.touched && sk.controls[imageOrDescription].errors?.[errorType]);
  }

  addStep() {
    const steps = <FormArray>this.recipeForm.get('steps');
    const stepGroup = new FormGroup({
      stepImage: new  FormControl('', Validators.required),
      stepDescription: new FormControl('', Validators.required)
    });
    steps.push(stepGroup);
  }

  resetSteps() {
    const steps = <FormArray>this.recipeForm.get('steps');
    steps.clear();
  }

  getStepControls() {
    const steps = <FormArray>this.recipeForm.get('steps');
    return steps.controls;
  }

  deleteStep(index: number) {
    const steps = <FormArray>this.recipeForm.get('steps');
    steps.controls.splice(index, 1);
    steps.value.splice(index, 1);
  }

  ngOnDestroy(): void {
    this.recipeUploadingSubscription.unsubscribe();
  }
}
