export class Recipe {
  constructor(
    public id: string,
    public name: string,
    public imageUrl: string,
    public description: string,
    public ingredients: string,
    public steps: [{[key: string]: string}]
  ) {}
}
