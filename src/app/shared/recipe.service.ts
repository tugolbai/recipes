import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Recipe } from './recipe.model';

@Injectable()

export class RecipeService {
  recipesChange = new Subject<Recipe[]>();
  recipesFetching = new Subject<boolean>();
  recipeUploading = new Subject<boolean>();
  recipeRemoving = new Subject<boolean>();

  private recipes: Recipe[] = [];

  constructor(private http: HttpClient) {}

  getRecipes() {
    return this.recipes.slice();
  }

  fetchRecipes() {
    this.recipesFetching.next(true);
    this.http.get<{ [id: string]: Recipe }>('https://plovo-57f89-default-rtdb.firebaseio.com/recipes.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const recipeData = result[id];
          return new Recipe(id, recipeData.name, recipeData.imageUrl, recipeData.description,
            recipeData.ingredients, recipeData.steps);
        });
      }))
      .subscribe(recipes => {
        this.recipes = recipes;
        this.recipesChange.next(this.recipes.slice());
        this.recipesFetching.next(false);
      }, () => {
        this.recipesFetching.next(false);
      });
  }

  fetchRecipe(id: string) {
    return this.http.get<Recipe | null>(`https://plovo-57f89-default-rtdb.firebaseio.com/recipes/${id}.json`)
      .pipe(map(result => {
          if (!result) {
            return null;
          }
        return new Recipe(id, result.name, result.imageUrl, result.description,
          result.ingredients, result.steps);
        }),
      );
  }

  addRecipe(recipe: Recipe) {
    const body = {
      name: recipe.name,
      imageUrl: recipe.imageUrl,
      description: recipe.description,
      ingredients: recipe.ingredients,
      steps: recipe.steps,
    };

    this.recipeUploading.next(true);

    return this.http.post('https://plovo-57f89-default-rtdb.firebaseio.com/recipes.json', body).pipe(
      tap(() => {
        this.recipeUploading.next(false);
      }, () => {
        this.recipeUploading.next(false);
      })
    );
  }

  editRecipe(recipe: Recipe) {
    this.recipeUploading.next(true);

    const body = {
      name: recipe.name,
      imageUrl: recipe.imageUrl,
      description: recipe.description,
      ingredients: recipe.ingredients,
      steps: recipe.steps,
    };

    return this.http.put(`https://plovo-57f89-default-rtdb.firebaseio.com/recipes/${recipe.id}.json`, body)
      .pipe(tap(() => {
          this.recipeUploading.next(false);
        }, () => {
          this.recipeUploading.next(false);
        })
      );
  }

  removeRecipe(id: string) {
    this.recipeRemoving.next(true);

    return this.http.delete(`https://plovo-57f89-default-rtdb.firebaseio.com/recipes/${id}.json`).pipe(
      tap(() => {
        this.recipeRemoving.next(false);
      }, () => {
        this.recipeRemoving.next(false);
      })
    );
  }
}
